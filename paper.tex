\documentclass[nonacm, sigconf]{acmart}

\settopmatter{printacmref=false}
\usepackage{listings}
\lstset{breaklines=true}
\settopmatter{printfolios=true}



\begin{document}

\title{Snapshot Analysis in Public Cloud Infrastructures}
\author{Niklas Zantner}
\affiliation{\institution{University of Passau}}
\email{mail@niklaszantner.de}

\begin{abstract}
  \textbf{Virtual machine introspection (VMI) defines a common way of investigating incidents in virtualized computing platforms pre and post of their occurrence. Amongst other things, it allows incident detection, without being part of a potentially compromised system. This paper examines the current state of VMI support by selected IaaS providers and shows, that none of them supports any way of hypervisor-based memory acquisition or supports any introspection of a running system. It further shows how VMI can be realized in IaaS controlled environments, by evaluating how a list of selected type 1 hypervisors. This is assessed via the hypervisor's support of nested virtualization and virtual machine introspection via LibVMI. Additionally, further sources of information about the state of the VM (via memory dump or VM snapshot) of those hypervisors will be examined in both public (IaaS) and private (on-premise) cloud environments.}
\end{abstract}

\keywords{Public Cloud, Snapshot Analysis, Memory Forensics, LibVMI, IaaS, Volatile Memory Introspection, Hypervisor Comparison, Nested Virtualization}

\maketitle




\section{Introduction}
Virtualization is a common technology used in production environments of today's computing infrastructure. It simplifies the management (scaling, deployment, replacement, etc.) while having minimal performance impacts. Additionally, the hypervisor allows analysing the virtualized or guest systems from the outside, a process which is called virtual machine introspection (VMI). VMI utilizes the hypervisor to assess the state of the guest system, the used memory, disk space or computing power while being decoupled from the actual guest. Even if the guest has been compromised trustworthy information can, therefore, be collected.

In private environments common tools and methods to allow VMI are well known and used, as affected machines can be identified and their memory easily accessed. The infrastructure is owned by the user and can, therefore, be fully controlled by the user including the hypervisor. Moving systems to public cloud or infrastructure as a service (IaaS) providers, on the other hand, gives this control to the providers as those are usually the only ones with full access to the hypervisor and infrastructure. Still, public cloud systems allow for a more cost-sensitive implementation of services by utilizing computing capacities, such as available computing power, storage space and network bandwidth more efficiently. Hence the number of IaaS deployed systems and the IaaS market itself has been rapidly growing in the last years and is projected to grow further \cite{GartnerIaaS}.

In any system regardless if deployed in a public or private cloud environment, several attributes have to be monitored, either as part of the technical dimension of digital forensics (key management, chain of custody of shared resources, logging of file access or volatile memory acquisition, etc. \cite{Pichan15}) or to analyse running systems for management purposes (memory, bandwidth, CPU usage, etc.). While the monitoring for management purposes can usually be realized via in-service solutions (see Amazon Cloudwatch for AWS as an example), more specific hypervisor-based control is not that easily available to the IaaS customer. The question of VMI support by IaaS providers arises, to allow their customers the same advantages by VMI as in private environments.

This paper aims to support researches and developers investigating virtualization-based systems after an incident occurs and evaluates strategies to improve incident investigations before those occur. We will focus on the volatile memory acquisition as part of VMI in running virtualized environments, as memory acquisition plays a key role in digital forensics, incident management and even pre-incident in the form of intrusion detection. This paper will also report on non-volatile or non-live data, therefore assessing disk-snapshot and memory dump capabilities of hypervisors and tools as additional sources of information. Anything that carries information about the internal state of the VM can be of interest.

The paper starts with giving an \textit{Overview about investigative tools} as those tools can be used either after an incident occurred or already beforehand. \textit{Support for accessing memory and storage per Hypervisor} is meant to give an insight into how memory or VM snapshots can be acquired by evaluating three type 1 hypervisors and two type 2 hypervisors and their LibVMI support. Those two questions apply in public and private cloud systems. The section \textit{Current state of in-service VMI support by selected IaaS providers} and as a follow-up \textit{Realizing VMI support without the IaaS provider} give additional context in the public cloud environments.




\section{Background}
This section is meant to give readers which are not yet into the context of virtualization a quick introduction into the basic terms. If you are already familiar with \textit{VM}, \textit{Hypervisor}, \textit{VMI} and the \textit{Semantic Gap Problem} you can skip this section.

\subsection{Virtual Machine (VM) and Hypervisors}
A VM allows running an operating system (OS) in a virtualized environment. A simple example is a Linux system which uses virtualization software to allow running a Windows OS as a guest inside of the Linux system. In general, a Hypervisor (also called Virtual Machine Monitor, VMM) provides the functionality of physical computing hardware to a guest OS and can even support running guests which have not been created to run on the physical hardware of the computer. Hypervisors manage the memory, processing and hardware in- and outputs, basically any aspect real hardware would offer to the guest OS.\newline
Two types of hypervisors exist. Type 1 hypervisors run directly on the host's hardware and are therefore also referred to as bare-metal or native hypervisors. Examples for those are XEN, KVM or Hyper-V. Type 2 hypervisors are running as a user-controlled program in the user space of the host operating system. They are a normal program exactly as your browser or text editor.


\subsection{Virtual machine introspection (VMI)}
VMI uses the managed capabilities of the hypervisor to allow introspection into the state of the guest system - without the guest system knowing about being watched. Especially the volatile memory (RAM) of the guest OS is interesting for researches and developers, as it shows what is currently happening, e.g. which processes are running, or what a potential attacker might have typed into a BASH session of a compromised system.


\subsection{Semantic Gap Problem}
The Semantic Gap Problem in the context of VMI is described as the missing semantic information to understand the raw memory data, which can be extracted from the guest via the hypervisor. Without knowing how the guest OS manages and structures the memory it uses, reverse engineering efforts are required to interpret the raw memory data correctly. Current VMI solutions depend on knowing the memory structures of the guest OS and that a potential attacker did not modify those (e.g. by recompiling the kernel used by the guest).




\section{Overview about investigative tools}
There are several tools available to researches and developers. We will give a short introduction to the LibVMI, Volatility, pyvmidbg and Rekall. If you are already familiar with those you can skip the following section.

\begin{itemize}
  \item \textbf{LibVMI}\newline
  LibVMI is a C based library to introspect the memory of running VMs or memory dumps of those. It provides a Python interface that allows to programmatically interact with the memory content of the VM, read and write to it and also allows to use Volatilities higher semantic capabilities and features on the captured memory. The library is developed as an open-source project at GitHub \cite{LibVMI} and there exists also an alternative implementation in Rust \cite{libvmiRUST}. An in detail report about LibVMI and its usage has been written by Bryan D. Payne \cite{Payne2012SimplifyingVM}.\newline

  \item \textbf{Volatility}\newline
  Volatility is "intended to introduce people to the techniques and complexities associated with extracting digital artefacts from volatile memory samples and provide a platform for further work into this exciting area of research" \cite{Volatility}. It includes several plugins which allow performing predefined tasks, such as listing the running processes, kernel modules or the bash history of the VM. The upcoming version 3 which is available as public-beta is a rewrite from scratch in Python 3 and amongst other things can automatically determine the memory profile necessary to read the memory dump \cite{Volatility3}. The framework is developed as an open-source project at GitHub \cite{VolatilityFoundation}.\newline

  \item \textbf{Rekall}\newline
  Rekall started as the development branch of Volatility but was forked and continued as an advanced framework. It can also be used as a library in existing projects, providing several user interfaces, including a web-based one similar to IPython notebook and "leverages exact debugging information provided by the operating system vendors to precisely locate significant kernel data structures" \cite{Rekall}. It can be run in live mode to investigate the system it is running on or can be used to analyse memory dumps. In combination with maintaining a list of memory profiles for several operating systems, it reduces the effort to start with memory analysis. Rekall can be combined with LibVMI to handle the memory acquisition and therefore enabling Rekall to analyse remote live systems as described by Mathieu Tarral \cite{RekallWithLibVMI}.\newline

  \item \textbf{pyvmidbg}\newline
  pyvmidbg allows you to connect a GDB session to a process running in a VM. Or as the GitHub README describes the project:
  \begin{quote}
  This GDB stub allows you to debug a remote process running in a VM with your favourite GDB frontend. \cite{pyvmidbg}
  \end{quote}
  This tool gives insides into the processes running in the VM and therefore allows live investigation and debugging of those by relying on LibVMI. pyvmidbg supports VirtualBox, KVM and XEN and is available as an open-source project at Github \cite{pyvmidbg}. \textit{Figure 1} shows the tools creators vision for the project:
  \begin{figure}[h]
  \centering
  \includegraphics[width=\linewidth]{./pyvmidbg.png}
  \caption{pyvmidbg vision with supported frontends and debuggers, taken from their Github page \cite{pyvmidbg}. Not everything as depicted is supported yet, but the project is under active development.}
  \Description{}
  \end{figure}

  \item \textbf{rVMI}\newline
  rVMI is based on a KVM kernel module, QEMU and Rekall \cite{RVMI}. The GitHub README describes it as:
  \begin{quote}
  rVMI is a debugger on steroids. It leverages Virtual Machine Introspection (VMI) and memory forensics to provide full system analysis. This means that an analyst can inspect userspace processes, kernel drivers, and pre-boot environments in a single tool. \cite{RVMI}
  \end{quote}
  On the downside, rVMI only works on Intel CPUs as it uses those virtualization extensions, does not support nested virtualization environments and has not been updated since 2017.
\end{itemize}




\section{Support for accessing memory and storage per Hypervisor}
Different sources to acquire information from within the virtualized systems are relevant to researches and developers, disk snapshot, memory dumps, accessing the memory managed by the hypervisor or anything about the internal state of a VM. This section assesses which of those information sources are available in selected type 1 and type 2 hypervisors, and informs about the LibVMI support to directly access the memory of a running VM. The latter allows analysing an incident, not in retrospective but while it is happening instead. Incidents can thereby be identified by utilizing the memory information to feed an intrusion detection system \cite{Lamps14}.

\begin{itemize}
  \item \textbf{Type 1 Hypervisors:}
  For all of the selected hypervisorsFull VM snapshots and also memory dumps can be created by libvirt library’s \texttt{dump} and \texttt{save} or \texttt{snapshot} \cite{libvirtDocs}.\newline

  \begin{itemize}
    \item \textbf{Hyper-V}\newline
    Saving the state of a VM in Hyper-V is called a \textit{Checkpoint} and can either contain both file and memory state of the VM (Standard Checkpoint) or a file snapshot only (Production Checkpoint). Both types can be created by using the Hyper-V Manager or using PowerShell \cite{MicrosoftHyperVDocs}.
    There is currently no support for live-investigation into running Hyper-V instances by LibVMI. Microsoft as the developer behind Hyper-V offers a command-line tool to allow memory acquisition \textit{vm2dmp}, which allows converting the saved state of a VM to be converted to a full memory dump \cite{VM2DMP}. Volatility’s imagecopy plugin can then be used to convert the created memory dump into a readable format to be used with Volatility \cite{VolatilityImageinfo}. This does not replace LibVMI but can give insides into the memory state of the VM.\newline

    \item \textbf{KVM}\newline
    For KVM creating snapshots relys on using libvirt or QEMU via the \texttt{qemu-img create} command with the \texttt{-b} flag \cite{QUEMUDocumentation}.
    LibVMI supports KVM if libvirt is available during compile time \cite{LibVMI}, native VMI support in KVM can be achieved by patching QEMU-KVM or enable GDB access. A detailed description can be found at the LibVMI GitHub repository \cite{LibVMI} or following the description of the KVM-VMI project \cite{KVMVMI}.\newline

    \item \textbf{XEN}\newline
    Creating memory dumps of a guest running in XEN can be done by using \texttt{xm dump-core} \cite{XenCoreDump}. To create full snapshot some kind of managing software is needed, as already mentioned libvirt can be used, or as an alternative the Citrix xe CLI command \texttt{xe vm-snapshot} \cite{CITRIXXenSnapshot}.
    Accessing the memory of a guest running live in XEN is supported out of the box by LibVMI \cite{LibVMI}. No further configuration, plugins or conversion is necessary, also the Xen version is dynamically detected at runtime, therefore LibVMI does not have to be recompiled when XEN is updated.\newline
  \end{itemize}

  \item \textbf{Type 2 Hypervisors:}
  \begin{itemize}
    \item \textbf{VirtualBox}\newline
    Full RAM dumps can be analyzed by LibVMI by using --dbg switch when starting a VM, which outputs a physical memory dump that can be analyzed by LibVMI \cite{VirtualBoxMemoryDump}. Direct LibVMI support does not exist for VirtualBox, but the IceBox project could be used instead \cite{IceBox}. Volatility support exists \cite{VolatilityVirtualBox} and snapshots can be created from within the VirtualBox Manager.\newline

    \item \textbf{VMware Workstation (Player)}\newline
    Integrating LibVMI with VMware Workstation was proposed by the community but is not yet available \cite{VMwareCommunityIntegratewithLibVMI}. As an alternative creating a snapshot of the VM can be achieved via the VM settings in the VMware Workstation application. The created snapshot can then be converted to a memory dump utilizing vmss2core tool provided by VMware. "Developers can use this tool to debug guest operating systems and applications by converting a virtual machine checkpoint into a core dump file" as described in the VMware Knowledge Base \cite{vmss2core}. This does not allow for live investigation of a running system but allows us to gain at least some insides into the machine.\newline
  \end{itemize}
\end{itemize}




\section{Current state of in-service VMI support by selected IaaS providers}
As the field of IaaS providers is growing only a subset of those can be evaluated. The following providers have been selected due to their popularity \cite{GartnerIaaS}:

\begin{itemize}
  \item Amazon Web Service (AWS)
  \item Google Compute Engine (GCE)
  \item Windows Azure (Azure)
\end{itemize}

All of the listed IaaS providers do support monitoring for management purposes (network, RAM usage and other metrics) to support their platform users. Their focus hereby lies on investigating performance and stability issues to enhance availability and resource usage efficiency, but not explicitly to help with digital forensics or giving advanced hypervisor access. Architectures to enable IaaS providers to offer forensic services have been the subject of studies by Reiser et. al. and Miyama et al. \cite{Zach15,Miyama17}. Still, neither of the listed providers allows an in-service VMI as it is currently possible with on-premise hosting. No in-service or out-of-the-box mechanism is available to access the memory managed by the hypervisor. This situation is already well known and has been investigated by several studies over the last few years \cite{Orr18,Alqahtany14,Zach15}, but has not yet changed.

As the monitoring interfaces of all of the examined services have a huge number of configuration levels and options, the author was trying to confirm his findings by asking the support of the listed services:
\begin{itemize}
  \item \textbf{AWS}\newline
  Dan Urson, a representative of AWS did answer after contacting the AWS security support via \href{mailto:aws-security@amazon.com}{\nolinkurl{aws-security@amazon.com}} and confirmed that no in-service solution does currently exist for AWS. As an alternative, he suggested trying a bare-metal IaaS provider. At least getting disk snapshots of EC2 AWS instances is supported by AWS by utilizing the underlying EBS volume \cite{diskSnapshotAWS}.\newline

  \item \textbf{Azure}\newline
  The Azure support referred to an article about diagnostic information collection.
  \begin{quote}
  The dump file is created by generating a Hyper-V save-state of the virtual machine (VM). This process will pause the VM for up to 10 minutes, after which time the VM is resumed. The VM is not restarted as part of this process. \cite{MicrosoftAzureSupport}
  \end{quote}
  Also, no reboot of the machine is required, this feature is only close to the defined requirements, as the process requires the instance to temporarily be not available, no memory acquisition of the system while running is possible. On the other hand, snapshots of virtual hard drives can be easily created via PowerShell or the Azure portal \cite{diskSnapshotAZURE}.\newline

  \item \textbf{GCE}\newline
  The author tried to contact Google via their official support service \url{cloud.google.com/support}and the only email address which was public in the Google Cloud Computing documentation \href{mailto:stackdriver-irm-support@google.com}{\nolinkurl{stackdriver-irm-support@google.com}}. The contact via email also referred to the official support service.\newline
  The google support service did not answer any inquiry.
  As with the other services, creating snapshots of the disks is supported and can be achieved via the Google Cloud Console \cite{diskSnapshotGCE}.\newline
\end{itemize}

The results of this investigation where all negative, none of the selected IaaS providers supports VMI in-service. Therefore the author tried to explicitly search for providers which allow to access the hypervisor layer or support VMI with some in-service interface. No service was found (apart from using nested virtualization, see \textit{Nested Virtualization in a Private Cloud environment}).




\section{Realizing VMI support without the IaaS provider}
As the problem of missing IaaS provider based support for VMI exists since IaaS providers exist, projects to mitigate this issue have been created. One popular open-source solutions is \textit{Margarita Shotgun} (100+ stars on GitHub \cite{MargaritaShotgun}).


\subsection{Margarita Shotgun}
\textit{Margarita Shotgun} is a python tool to acquire memory from remote AWS instances by utilizing the Linux Memory Extractor (LiME) loadable kernel module on this instance. LiME extends the running kernel and allows to extract volatile memory from within a running Linux or Linux-based machine (e.g. Android). As the creators of \textit{Margarita Shotgun} state:
\begin{quote}
  After determining the remote system's kernel version and architecture the appropriate LiME LiME kernel module is loaded and system memory is streamed over an ssh tunnel to the incident responder's workstation. Memory can be saved to disk or streamed directly to an s3 bucket \cite{MargaritaShotgunOverview}.
\end{quote}
The tool does not only allow us to capture memory from running instances but also secures the transfer of the dump via ssh. It can be used via a command-line interface or can be imported in a custom python script to automate its supported operations \cite{MargaritaShotgunDocs}.

Following lines show an example of the command line usage, downloading the dump to the executing users home directory:
\begin{lstlisting}[basicstyle=\small]
margaritashotgun --server IP_ADRESS --username EC2-USER --key KEY --output-dir ~ --repository
\end{lstlisting}

The resulting memory dump file can then be analysed with the already mentioned tools. The disadvantage of Margarita Shotgun is that it is relying on a kernel module running in the guest system, therefore this method of memory acquisition is not as decoupled from the potentially compromised guest compared to using only hypervisor-based functionality.


\subsection{Nested Virtualization in a Pubic Cloud environment}
As shown in the previous section IaaS providers do not support virtual machine introspection out of the box, as they only allow their users to access a limited set of hypervisor functionality. But as the disadvantage of Margarita Shotgun shows, working without full hypervisor support, results in a relying on a potentially compromised guest. An alternative is to use nested virtualization, by setting up your hypervisor and virtualization environment in an already virtualized environment. Nested virtualization is supported by all of the IaaS providers evaluated in this paper, also with limitations \cite{nestedSupportGCE,nestedSupportAWS,nestedSupportAzure}.

There are two relevant properties of nested virtualization in the context fo this paper. The negative property is the performance loss as at least two layers of virtualization create an additional overhead \cite{Yehuda10,Beham13}. The positive property is that the nested hypervisor can be fully controlled by the users, therefore they can use all available tools and functionalities the hypervisors provide to access the memory managed by it.

The VM state acquisition capabilities of the hypervisors have been already covered in \textit{Support for accessing memory and storage per Hypervisor}. Those capabilities of a hypervisor are not depending on if the hypervisor is deployed in a nested or non-nested environment. Generally speaking, the same techniques which are used in nested hypervisor-based systems in an IaaS context may also be used in non-nested on-premise systems, again because a nested or non-nested hypervisor can be fully controlled by the users. The next step is to evaluate the hypervisors support of nested virtualization. Only type 1 hypervisors are evaluated, as those are used in private and public cloud environments to realize the first virtualization level:

\begin{itemize}
  \item \textbf{Hyper-V}\newline
  Hyper-V can offer nesting as long as the host and guest are Windows Server 2016 or Windows 10 Anniversary Update based systems and the used hardware is a current Intel processor supporting VT-x and EPT. The latter might be a limitation but ensures that hardware features to improve performance and security are available and increase the service quality \cite{nestedSupportHyperV}.\newline

  \item \textbf{KVM}\newline
  Since February 2018 KVM supports running KVM guests in KVM guests, also the feature is still considered experimental. Still, Google explicitly mentions the KVM nesting in their nesting documentation \cite{nestedSupportGCE}. A known limitation is a missing migration, saving or loading of level 1 guests, as long as at least one level 2 guest is running in it \cite{nestedSupportKVM}. Performance optimizations and support of other hypervisors as hosts of a nested KVM is an ongoing topic in Linux kernel development \cite{updatesKVM}.\newline

  \item \textbf{XEN}\newline
  The XEN project wiki deems the nesting support to be ready to use since 2017 and supplies an extensive list of tested hypervisor guests and hardware combinations. The limitation also applies with XEN, using hardware extensions (Hardware Virtual Machine, HVM) as VT-x by Intel are considered as "tech preview" of Xen 4.4 and their usage is "not recommend that it be used in a production environment at this time" \cite{nestedSupportXEN}.\newline
\end{itemize}

To summarize, all of the selcted IaaS providers do support nested virtualization. Therefore this approach is feasible, at the cost of reducing performance.\newline
One final suggestion is to deploy only one VM in the nested hypervisor. \textit{Wrapping} the actual VM carrying the application in the nested hypervisor allows developers to still use the management interfaces (e.g. for deploying, halting, etc.) enabled by the IaaS provider. Deploying multiple of those application VMs in one nested hypervisor would force developers to manage the deployed instances in two levels, once via the IaaS provider and once via the management interface of the nested hypervisor, resulting in additional operational overhead and costs.




 \section{Related Work}
 Miyama et al. propose nested-virtualization based VMI architecture which even allows mitigating the actions of potentially hostile VMs and potentially hostile administrators \cite{Miyama17}. Their approach could be used as the groundwork to allow IaaS providers to offer VMI services as an additional security feature if you see the user of IaaS services as the "potentially hostile VMs and potentially hostile administrators".
 Zhang et al. are using nested virtualization for a similar purpose. Their prototype called \textit{CloudVisor} also separates the management of client VMs from their security oversight, but does not utilize any VMI technique \cite{Zhang11}.
 Reiser et al. present the \textit{LiveCloudInspector} architecture to enable IaaS providers to offer (memory) forensic services within their products. The architecture is explicitly being aimed at memory and network forensics efforts within live systems, while being independent of the underlying cloud platform \cite{Zach15}.
 Reiser et al. also presents a research project evaluating "Intrusion Detection and Honeypots in Nested Virtualization Environments". It yields detailed insights into the hypervisor performance while running nested systems and the performance impacts of running an intrusion detection system. While focusing on virtual machines in IaaS clouds at the time the paper was written no IaaS provider supported nested virtualization \cite{Beham13}.





 \section{Conclusion}
 Accessing disk snapshots or memory dumps as non-live sources of information about the internal state of a guest is possible with the right tools (e.g. libvirt). Live memory acquisition on a running IaaS systems can currently only be realized by relying on special kernel modules as with Margarita Shotgun or using a nested virtualization setup, as none of the assessed IaaS providers does allow to use VMI. Also explicitly searching for an IaaS provider allowing VMI did not yield any result. Hopefully, as the topic of memory forensics in cloud environments grows further or regulatory compliance is forced upon companies, IaaS providers enable their customers to do so.




\bibliographystyle{ACM-Reference-Format}
\bibliography{./sources.bib}

\end{document}
\endinput
